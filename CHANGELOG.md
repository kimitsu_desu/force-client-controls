# Force Client Controls

## Release 1.1.0

- Add escape mode. The GM may Alt-click the lock icon to escape the forced bindings and adjust them privately.

## Release 1.0.5

- Fix module manifest for v10 and v11 compatibility
- Fix control bindings configuration app not showing actual bindings after locking/unlocking soft-forced bindings on clients
- Change open and soft-lock icons for clarity

## Release 1.0.4

- Update for v10 compatibility

## Release 1.0.3

- Fix cannot edit locked controls as a GM

## Release 1.0.2

- Fix incorrect handling of uneditable bindings
- Minor stability improvements

## Release 1.0.1

- Remove debug messages
- Fix controls not being forced for the GM
- Disable controls editing for clients when forced

## Release 1.0.0

- Initial release
