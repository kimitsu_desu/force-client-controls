class ForceClientControls {
  static moduleName = "force-client-controls";
  static forced = new Map();
  static unlocked = new Map();

  static isGM(defaultResult = false) {
    if (game.user) {
      return game.user.role >= CONST.USER_ROLES.GAMEMASTER;
    } else {
      try {
        return game.data.users.find((u) => u._id === game.userId).role >= CONST.USER_ROLES.GAMEMASTER;
      } catch {
        return defaultResult;
      }
    }
  }

  static initialize() {
    game.settings.register(ForceClientControls.moduleName, "forced", {
      name: `${game.i18n.localize("FORCECLIENTCONTROLS.settings.forced.name")}`,
      hint: `${game.i18n.localize("FORCECLIENTCONTROLS.settings.forced.hint")}`,
      scope: "world",
      config: false,
      type: Object,
      default: {},
    });
    game.settings.register(ForceClientControls.moduleName, "unlocked", {
      name: `${game.i18n.localize("FORCECLIENTCONTROLS.settings.unlocked.name")}`,
      hint: `${game.i18n.localize("FORCECLIENTCONTROLS.settings.unlocked.hint")}`,
      scope: "client",
      config: false,
      type: Object,
      default: {},
    });

    ForceClientControls.forced = new Map(Object.entries(game.settings.get(ForceClientControls.moduleName, "forced")));
    ForceClientControls.unlocked = new Map(
      Object.entries(game.settings.get(ForceClientControls.moduleName, "unlocked"))
    );

    libWrapper.register(
      ForceClientControls.moduleName,
      "KeybindingsConfig.defaultOptions",
      ForceClientControls.keybindingsConfigDefaultOptions,
      "WRAPPER"
    );

    libWrapper.register(
      ForceClientControls.moduleName,
      "ClientKeybindings.prototype.initialize",
      ForceClientControls.bindingsInitialize,
      "WRAPPER"
    );
    libWrapper.register(
      ForceClientControls.moduleName,
      "ClientKeybindings.prototype.resetDefaults",
      ForceClientControls.bindingsResetDefaults,
      "WRAPPER"
    );
    libWrapper.register(
      ForceClientControls.moduleName,
      "ClientKeybindings.prototype.get",
      ForceClientControls.bindingsGet,
      "WRAPPER"
    );
    libWrapper.register(
      ForceClientControls.moduleName,
      "ClientKeybindings.prototype.set",
      ForceClientControls.bindingsSet,
      "WRAPPER"
    );
    Hooks.on("renderKeybindingsConfig", ForceClientControls.renderKeybindingsConfig);
  }

  static keybindingsConfigDefaultOptions(wrapped, ...args) {
    return foundry.utils.mergeObject(wrapped(...args), { scrollY: [".category-list", ".scrollable"] });
  }

  static isForced(action) {
    return ForceClientControls.forced.has(action)
      && (
        ForceClientControls.isGM() && !ForceClientControls.unlocked.has(action)
        || !ForceClientControls.isGM() && (ForceClientControls.forced.get(action).mode !== "soft" || !ForceClientControls.unlocked.has(action))
      );
  }

  static async saveForced() {
    await game.settings.set(
      ForceClientControls.moduleName,
      "forced",
      Object.fromEntries(ForceClientControls.forced)
    );
  }

  static async saveUnlocked() {
    await game.settings.set(
      ForceClientControls.moduleName,
      "unlocked",
      Object.fromEntries(ForceClientControls.unlocked)
    );
  }

  // This function quotes from foundry.js ClientKeybindings.initialize and has to be maintained to reflect changes
  static bindingsInitialize(wrapped, ...args) {
    const result = wrapped(...args);
    // Register bindings for forced actions
    for (let [action, config] of this.actions) {
      if (ForceClientControls.isForced(action)) {
        let bindings = config.uneditable.concat(ForceClientControls.forced.get(action).bindings ?? config.editable);
        console.log(`Force Client Controls | Forcing ${action}`);
        this.bindings.set(action, bindings);
      }
    }

    // Create a mapping of keys which trigger actions
    this.activeKeys = new Map();
    for (let [key, action] of this.actions) {
      let bindings = this.bindings.get(key);
      for (let binding of bindings) {
        if (!binding) continue;
        if (!this.activeKeys.has(binding.key)) this.activeKeys.set(binding.key, []);
        let actions = this.activeKeys.get(binding.key);
        actions.push({
          action: key,
          key: binding.key,
          name: action.name,
          requiredModifiers: binding.modifiers,
          optionalModifiers: action.reservedModifiers,
          onDown: action.onDown,
          onUp: action.onUp,
          precedence: action.precedence,
          order: action.order,
          repeat: action.repeat,
          restricted: action.restricted,
        });
        this.activeKeys.set(binding.key, actions.sort(this.constructor._compareActions));
      }
    }
    return result;
  }

  static async bindingsSet(wrapped, namespace, action, bindings, ...args) {
    if (!namespace || !action) throw new Error("You must specify both namespace and key portions of the keybind");
    const actName = `${namespace}.${action}`;
    if (ForceClientControls.isForced(actName)) {
      const forced = ForceClientControls.forced.get(actName);
      if (ForceClientControls.isGM()) {
        ForceClientControls.forced.set(actName, { ...forced, bindings: bindings });
        const result = await wrapped(namespace, action, bindings, ...args);
        await ForceClientControls.saveForced();
        return result;
      } else {
        bindings = forced.bindings;
      }
    }
    return await wrapped(namespace, action, bindings, ...args);
  }

  static bindingsGet(wrapped, namespace, action, ...args) {
    let bindings = wrapped(namespace, action, ...args);
    const actName = `${namespace}.${action}`;
    if (ForceClientControls.isForced(actName)) {
      bindings = ForceClientControls.forced.get(actName).bindings || [];
    }
    return bindings;
  }

  static async bindingsResetDefaults(wrapped, ...args) {
    ForceClientControls.forced.clear();
    ForceClientControls.unlocked.clear();
    await ForceClientControls.saveForced();
    await ForceClientControls.saveUnlocked();
    return await wrapped(...args);
  }

  static async forceBinding(action, mode) {
    if (!ForceClientControls.isGM()) return;
    ForceClientControls.forced.set(action, {
      mode: mode,
      bindings: game.keybindings.bindings
        .get(action)
        ?.filter((elem) => !game.keybindings.actions.get(action)?.uneditable?.includes(elem)),
    });
    await ForceClientControls.saveForced();
    game.keybindings.initialize();
  }

  static async unforceBinding(action) {
    if (!ForceClientControls.isGM()) return;
    ForceClientControls.forced.delete(action);
    await ForceClientControls.saveForced();
    game.keybindings.initialize();
  }

  static async unlockBinding(action) {
    if (ForceClientControls.forced.has(action)) {
      const forced = ForceClientControls.forced.get(action);
      if (forced.mode === "soft" || ForceClientControls.isGM()) {
        ForceClientControls.unlocked.set(action, true);
        await ForceClientControls.saveUnlocked();
        game.keybindings.initialize();
      }
    }
  }

  static async lockBinding(action) {
    if (ForceClientControls.forced.has(action)) {
      const forced = ForceClientControls.forced.get(action);
      if (forced.mode === "soft" || ForceClientControls.isGM()) {
        ForceClientControls.unlocked.delete(action);
        await ForceClientControls.saveUnlocked();
        game.keybindings.initialize();
      }
    }
  }

  static async renderKeybindingsConfig(app, html) {
    while (!app.rendered) {
      await new Promise((resolve) => resolve());
    }

    const isGM = ForceClientControls.isGM();
    const fa = {
      "hard-gm": "fa-lock",
      "soft-gm": "fa-unlock-keyhole",
      "open-gm": "fa-lock-keyhole-open",
      "unlocked-gm": "fa-dungeon",
      "hard-client": "fa-lock",
      "soft-client": "fa-unlock-keyhole",
      "unlocked-client": "fa-lock-keyhole-open",
    };

    const elem = html.find(".action");
    elem.each(function() {
      const action = $(this).attr("data-action-id");
      let mode = ForceClientControls.forced.get(action)?.mode ?? "open";
      if ((mode === "soft" || isGM) && ForceClientControls.unlocked.has(action)) mode = "unlocked";
      mode += isGM ? "-gm" : "-client";
      if (mode !== "open-client") {
        $(this)
          .find("h4, label")
          .first()
          .prepend(
            $("<span>")
              .html("&nbsp;")
              .prop("title", game.i18n.localize(`FORCECLIENTCONTROLS.ui.${mode}-hint`))
              .addClass(`fas ${fa[mode]}`)
              .click(async function(e) {
                await ForceClientControls.clickToggleForceControls(e, action, app);
              })
          );
      }
      if (["hard-client", "soft-client"].includes(mode)) {
        $(this).find("a").css({ "pointer-events": "none", cursor: "default", color: "gray" });
      }
    });
  }

  static async clickToggleForceControls(e, action, app) {
    if (ForceClientControls.isGM()) {
      await app._savePendingEdits();
      let mode = ForceClientControls.forced.get(action)?.mode ?? "open";
      if (mode === "open") {
        await ForceClientControls.forceBinding(action, "soft");
      } else if (ForceClientControls.unlocked.has(action)) {
        await ForceClientControls.lockBinding(action);
      } else if (e.altKey) {
        await ForceClientControls.unlockBinding(action);
      } else if (mode === "soft") {
        await ForceClientControls.forceBinding(action, "hard");
      } else {
        await ForceClientControls.unforceBinding(action);
      }
    } else {
      if (ForceClientControls.forced.get(action)?.mode === "soft") {
        await app._savePendingEdits();
        if (ForceClientControls.unlocked.has(action)) {
          await ForceClientControls.lockBinding(action);
        } else {
          await ForceClientControls.unlockBinding(action);
        }
      }
    }
    // _savePendingEdits clears internal #chachedData, then re-render to reflect new setting values
    await app._savePendingEdits();
    app.render();
  }
}

Hooks.once("libWrapper.Ready", ForceClientControls.initialize);
