# Force Client Controls

Allows to force chosen control bindings for all clients to the defaults provided by the GM.

## Overview

This module adds lock icons next to the control bindings in the controls cofiguration app. Clicking or Alt-clicking these icons changes the behavior of the bindings:

- "Unlocked" icons mean that the control bindings are free to be changed by the individual clients.
- "Half-locked (with visible keyhole)" icons mean that the control bindings will be defaulted to those set by the GM, however any client may optionally "unlock" these bindings and adjust them if necessary.
- "Locked (with no visible keyhole)" icons mean that the control bindings will be forced to those set by the GM and may not be changed by clients.
- "Gate" (Alt-click the lock icon to activate) icons mean that the control bindings are forced or soft-forced for clients, however the GM has escaped the force and is able to adjust these bindings privately.

This allows the GM to either force chosen keybindings to ensure they will not be altered, or "soft-force" other keybindings that the GM finds helpful to be set to new defaults, but still let the players alter them if they choose to.
The GM may also set the defaults for clients, but then adjust their own bindings privately by Alt-clicking the lock icons and escaping the force.

![Example](https://gitlab.com/kimitsu_desu/force-client-controls/-/raw/master/example.jpg?inline=true)

## Current Limitations

- The clients may be required to refresh their browser before any alterations to the forced keybindings made by the GM will take effect.
- Resetting the control bindings to the default settings as a GM would also unlock all forced and "soft-forced" bindings.

## Compatibility and issues

## Troubleshooting

## License

MIT License (c) 2022 kimitsu
